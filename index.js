const express = require('express');
const braintree = require("./routes/braintree");

const app = express();
app.use(express.json());
app.use('/', braintree);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`listening on port ${port}....`);
})
