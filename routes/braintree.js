const express = require("express");
const braintree = require("braintree");
const router = express.Router();

const gateway = new braintree.BraintreeGateway({
    environment: braintree.Environment.Sandbox,
    merchantId: "YOUR MERCHANT ID",
    publicKey: "YOUR PUBLIC KEY",
    privateKey: "YOUR PRIVATE KEY"
});

/**
 * Request to get the client token
 */
router.get("/client_token", (req, res) => {
    gateway.clientToken.generate({}).then(response => {
        res.send(response.clientToken);
    });
});

/**
 * Make a sale based on the paymentMethodToken or a one time sale based on payment method nonce.
 */
router.post("/sale", (req, res) => {
    const paymentMethodNonce = req.body.paymentNonce;
    const paymentMethodToken = req.body.paymentMethodToken;
    const amount = req.body.amount;
    if(!paymentMethodNonce && !paymentMethodToken) {
        res.status(400).send("paymentMethodNonce or Token required to make a sale.");
        return;
    }
    if(paymentMethodNonce) {
        gateway.transaction.sale({
            amount,
            paymentMethodNonce,
            options: {
                submitForSettlement: true,
                storeInVaultOnSuccess: true
            }
        }).then(result => {
            if(result.success) {
                res.status(201).send(result);
            } else {
                res.status(500).send(result);
            }
        }).catch(err => {
            res.status(500).send(err);
        });
    } else {
        gateway.transaction.sale({
            amount,
            paymentMethodToken,
            options: {
                submitForSettlement: true
            }
        }).then(result => {
            if(result.success) {
                res.status(201).send(result);
            } else {
                res.status(500).send(result);
            }
        }).catch(err => {
            res.status(500).send(err);
        });
    }
});

/**
 * Create a payment method and assign it to the provided customerId
 */
router.post("/paymentMethod", (req, res) => {
    gateway.paymentMethod.create({
        customerId: req.body.customerId,
        paymentMethodNonce: req.body.paymentMethodNonce
    }).then(result => {
        if(result.success) {
            res.status(201).send(result);
        } else {
            res.status(500).send("Something went wrong");
        }
    }).catch(err => {
        res.status(500).send(err);
    })
});

module.exports = router;
